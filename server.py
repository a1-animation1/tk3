import socket
import threading
import _thread
import os
import sys
import pprint

class ConnectionStatus():
    def __init__(self, conn, status):
        self.conn = conn
        self.status = status


HEADER = 64
PORT = 5000
SERVER = socket.gethostbyname(socket.gethostname()) #your.private.ip.addr
ADDR = (SERVER, PORT)
FORMAT = 'utf-8'
DISCONNECT_MSG='!DISCONNECT'
conn_collection=dict()
job_collection=dict()
thread_collection=[]
queue = []
conn_status = dict()
checked = 0
active_connection = 0

server = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
server.bind(ADDR)

def send(conn, msg):
    message = msg.encode(FORMAT)
    msg_length = len(message)
    send_length = str(msg_length).encode(FORMAT)
    send_length += b' ' * (HEADER-len(send_length))
    conn.send(send_length)
    conn.send(message)

def execute_job(conn, filename, jobNum):
    send(conn, "job " + str(jobNum))
    with open(filename) as f:
        data = f.read()
    data_length = len(data)
    send_length = str(data_length).encode(FORMAT)
    send_length += b' ' * (HEADER-len(send_length))
    conn.send(send_length)
    conn.send(str(data).encode(FORMAT))

def command_listener(conn):
    global checked
    global active_connection
    while True:
        try:
            msg_length = int(conn.recv(HEADER).decode(FORMAT))
            msg = conn.recv(msg_length).decode(FORMAT)
            if msg.split()[0] == "job":
                jobNum = msg.split()[1]
                try:
                    msg_length = int(conn.recv(HEADER).decode(FORMAT))
                    msg = conn.recv(msg_length).decode(FORMAT)
                    status_job = conn.recv(50).decode(FORMAT)
                    if  status_job=='failed':
                        filename = "error job ke-" + jobNum
                        with open(filename + '.txt', 'w') as f:
                            f.write(msg)
                        job_collection[jobNum] = (job_collection[jobNum][0], "Failed")
                        print("Job " + jobNum + " has failed")
                    else:
                        filename = "output job ke-" + jobNum
                        with open(filename + '.txt', 'w') as f:
                            f.write(msg)
                        job_collection[jobNum] = (job_collection[jobNum][0], "Finished")
                        print("Job " + jobNum + " has been finished by " + str(conn_collection[conn]))
                except:
                    filename = "error job ke-" + jobNum
                    with open(filename + '.txt', 'w') as f:
                            f.write("ConnectionError")
                    job_collection[jobNum] = (job_collection[jobNum][0], "Failed")
                    print("Job " + jobNum + " has failed")
        
            elif msg.split()[0] == "status":
                conn_status[conn] = msg.split()[1]
                checked += 1

        except Exception:
            print("Connection to worker " + str(conn_collection[conn]) + " has been terminated")
            active_connection -= 1
            print(f"[LOG] Active connections: {active_connection}")
            for jobNum in job_collection.keys():
                job = job_collection[jobNum]
                if str(job[0]) == str(conn_collection[conn]) and job[1] == "Running":
                    print("Job " + jobNum + " has failed")
                    job_collection[jobNum] = (job[0], "Failed")
            break
        
def connection_listener():
    global active_connection
    while True:
        conn, addr = server.accept()
        conn_collection[conn] = addr
        queue.append(conn)
        status = conn.recv(HEADER).decode(FORMAT)
        if status == "n":
            for worker in conn_collection.keys():
                worker.close()
            break
        print("Worker " + str(addr) + " has joined")
        active_connection += 1
        print(f"[LOG] Active connections: {active_connection}")

        threading.Thread(target=command_listener, args=(conn,)).start()

def status_checker():
    global checked
    checked = 0
    for conn in conn_collection.keys():
        if ((str(conn_collection[conn]), "Running") in job_collection.values()):
            conn_status[conn] = "Running"
            checked += 1
        else:
            data = "status"
            data_length = len(data)
            send_length = str(data_length).encode(FORMAT)
            send_length += b' ' * (HEADER-len(send_length))
            try:
                conn.send(send_length)
                conn.send(str(data).encode(FORMAT))
            except Exception:
                conn_status[conn] = "Dead"
                checked += 1

def start():
    server.listen()
    print("Server started")
    thread = threading.Thread(target=connection_listener)
    thread.start()

    jobNum = 1
    global checked

    while True:
        serverInput = input().lower().split()
        if serverInput[0] == "exit":
            exitConn = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
            exitConn.connect(ADDR)
            exitConn.send(str("n").encode(FORMAT))
            exit()
        
        elif serverInput[0] == "send":
            filename = serverInput[1]
            
            conn = None
            busy_queue = []
            
            status_checker()
            while checked < len(conn_collection):
                pass

            for conn_candidate in queue:
                if conn_status[conn_candidate] == "Running":
                    conn = conn_candidate
                    queue.remove(conn)
                    queue.append(conn)
                    break
                elif conn_status[conn_candidate] == "Busy":
                    busy_queue.append(conn_candidate)
                    continue
                elif conn_status[conn_candidate] == "Dead":
                    queue.remove(conn_candidate)
                    continue
                else:
                    conn = conn_candidate
                    queue.remove(conn)
                    queue.append(conn)
                    break 
            
            for conn_candidate in busy_queue:
                queue.remove(conn_candidate)
                queue.append(conn_candidate)

            if len(queue) == 0 or conn == None:
                print("No Worker Available")
                continue    
            
            job_collection[str(jobNum)] = (str(conn_collection[conn]), "Running")
            print("Your Job Number is " + str(jobNum) + " and it's being worked on worker " + str(job_collection[str(jobNum)][0]))
            
            thread = threading.Thread(target=execute_job, args=(conn, filename, jobNum))
            thread.start()

            jobNum += 1

        elif serverInput[0] == "job" and len (serverInput) > 1:
            if serverInput[1] == "all":
                if len(job_collection) == 0:
                    print("No job exist")
                    continue
                for num in job_collection.keys():
                    print("Job: {}. Worker:{}. Status: {}.".format(num, str(job_collection[num][0]), job_collection[num][1]))
                    
            if serverInput[1].isnumeric():
                if serverInput[1] in job_collection.keys():
                    worker, status = job_collection[serverInput[1]]
                    print("Job: {}. Worker:{}. Status: {}.".format(serverInput[1], str(worker), status))

                else:
                    print("Job doesn't exist")
        
        elif serverInput[0] == "status":
            print(f"[LOG] Active connections: {active_connection}")

            if len(conn_collection) == 0:
                print("No worker exits")
                continue

            status_checker()
            print("Fetching data...")
            while checked < len(conn_collection):
                pass
            for conn in conn_collection.keys():
                try:
                    print("Worker " + str(conn_collection[conn]) + " is " + conn_status[conn])
                except:
                    print("Worker " + str(conn_collection[conn]) + " is Unknown")
    
print("[LOG] Server is starting...")
start()
