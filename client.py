import socket
import sys
import subprocess
import threading
import _thread
import time
import os

HEADER = 64
PORT = 5000
FORMAT = 'utf-8'
DISCONNECT_MSG = '!DISCONNECT'
SERVER = socket.gethostbyname(socket.gethostname()) #"your.public.ip.addr"
ADDR = (SERVER, PORT)
BUSY_FLAG=False
RUN_FLAG=False
TIMEOUT = 300

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect(ADDR)
client.send(str("y").encode(FORMAT))

job_queue = []

def send(msg):
    message = msg.encode(FORMAT)
    msg_length = len(message)
    send_length = str(msg_length).encode(FORMAT)
    send_length += b' ' * (HEADER-len(send_length))
    client.send(send_length)
    client.send(message)

def statusCheck():
    if not BUSY_FLAG:
        send(f"status Idle")
    else:
        send(f"status Busy")

def execute_job():
    start_timer = time.time()
    global connected
    global RUN_FLAG
    global TIMEOUT 
    while connected:
        if (time.time() - start_timer > TIMEOUT):
            print("TIMEOUT: Client has been idling too long.")
            client.close()
            connected = False
            break
        elif len(job_queue) == 0:
            continue

        try:
            RUN_FLAG = True
            job = job_queue.pop(0)
            jobNum = job[0]
            msg = job[1]

            filename = "Job ke-" + jobNum + ".py"

            with open(filename, 'w') as f:
                f.write(msg)

            print("Running job " + jobNum)

            msg = subprocess.check_output([sys.executable, filename]).decode(FORMAT)
            msg_length = len(msg)
            send_length = str(msg_length).encode(FORMAT)
            send_length += b' ' * (HEADER-len(send_length))

            send("job " + str(jobNum))
            client.send(send_length)
            client.send(str(msg).encode(FORMAT))
            client.send('success'.encode(FORMAT))
            os.remove(filename)
            RUN_FLAG = False
            print("Job " + jobNum +" has finished")
        except Exception as e:
            msg = str(type(e).__name__)
            msg_length = len(msg)
            send_length = str(msg_length).encode(FORMAT)
            send_length += b' ' * (HEADER-len(send_length))
            
            send("job " + str(jobNum))
            client.send(send_length)
            client.send(msg.encode(FORMAT))
            client.send('failed'.encode(FORMAT))
            os.remove(filename)
            print(type(e).__name__)
            print("Job " + jobNum +" has failed")
            RUN_FLAG = False
        
        #reset timer
        start_timer = time.time()

def command_listener():
    global connected
    while True:
        try:
            msg_length = int(client.recv(HEADER).decode(FORMAT))
            msg = client.recv(msg_length).decode(FORMAT)
            if msg == 'status':
                status_checker = threading.Thread(target=statusCheck)
                status_checker.start()
            elif msg.split()[0] == 'job':
                jobNum = msg.split()[1]
                msg_length = int(client.recv(HEADER).decode(FORMAT))
                msg = client.recv(msg_length).decode(FORMAT)
                job_queue.append((jobNum, msg))
        except:
            print("Connection to server has been terminated")
            print("Press enter to close")
            client.close()
            connected = False
            break
    

connected = True
threading.Thread(target=command_listener).start()
threading.Thread(target=execute_job).start()
while connected:
    command = input().lower()

    if not connected:
        break

    if (command == "busy"):
        if len(job_queue) != 0:
            print("Cannot be busy while job is running")
        else:
            BUSY_FLAG = True
            print("Status set to Busy!")

    elif (command == "active"):
        BUSY_FLAG = False
        print("Status set to Active!")

    elif (command == "status"):
        if BUSY_FLAG:
            print("Busy")
        elif RUN_FLAG:
            print("Running")
        else:
            print("Idle")

    elif (command == "queue"):
        for job in job_queue:
            print("Job " + job[0] + " is in queue")

    elif (command == "exit"):
        if RUN_FLAG == True:
            print("Cannot close while job is running")
            continue
        client.close()
        connected = False

print("Closing Client")